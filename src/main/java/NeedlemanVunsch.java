import Tables.Tables;

public class NeedlemanVunsch {
    char[] a, b;
    private int[][] ans;
    private static int d = -5;

    public NeedlemanVunsch(char[] a, char[] b) {
        this.a = a;
        this.b = b;
        ans = new int[a.length][b.length];
    }

    public NeedlemanVunsch calculateEvaluation() {
        int match, delete, insert;
        for (int i = 0; i < a.length; i++) {
            ans[i][0] = d * i;
        }
        for (int i = 0; i < b.length; i++) {
            ans[0][i] = d * i;
        }
        for (int i = 1; i < a.length; i++) {
            for (int j = 1; j < b.length; j++) {
                match = ans[i-1][j-1] + Tables.getScore(a[i], b[j]);
                delete = ans[i-1][j] + d;
                insert = ans[i][j-1] + d;
                ans[i][j] = Math.max(Math.max(match, delete), insert);
            }
        }
        return this;
    }

    public int[][] getMatrix() {
        return this.ans;
    }

    public char[] findChange() {
        String alignmentA, alignmentB;
        alignmentA = "";
        alignmentB = "";
        int i = a.length - 1;
        int j = b.length - 1;
        while (i > 0 && j > 0)
        {
            int score = ans[i][j];
            int scoreDiag = ans[i-1][j-1];
            int scoreUp = ans[i][j - 1];
            int scoreLeft = ans[i - 1][j];
            if(score == scoreDiag + Tables.getScore(a[i], b[j])) {
                alignmentA = a[i] + alignmentA;
                alignmentB = b[j] + alignmentB;
                i--;
                j--;
            } else {
                if(score == scoreLeft + d) {
                    alignmentA = a[i] + alignmentA;
                    alignmentB = "-" + alignmentB;
                    i--;
                } else {
                    if (score == scoreUp + d) {
                        alignmentA = "-" + alignmentA;
                        alignmentB = b[j] + alignmentB;
                        j--;
                    }
                }
            }
        }
        while (i >= 0) {
            alignmentA = a[i] + alignmentA;
            alignmentB = "-" + alignmentB;
            i--;
        }
        while (j >= 0) {
            alignmentA = "-" + alignmentA;
            alignmentB = b[j] + alignmentB;
            j--;
        }
        return alignmentA.toCharArray();
    }
}
