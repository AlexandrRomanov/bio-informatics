import java.util.ArrayList;

import static java.lang.Math.pow;

public class NeedlemanVunschForMultipleStrings {

    private ArrayList<char[]> matrixOfStrings = null;
    private int[] scoreMatrix = null;
    private int sizeOfString = 0;
    private int numOfStrings = 0;

    public int getElemOfScoreMatrix(int[] indexes) {
        int index = 0;
        for (int i = 0; i < numOfStrings; i++) {
            index = index + indexes[i] * ((int)pow(sizeOfString, i));
        }
        return index;
    }

    public NeedlemanVunschForMultipleStrings(int size, int num) {
        this.sizeOfString = size;
        this.numOfStrings = num;
        this.scoreMatrix = new int[(int) pow(size, num)];
    }

    public NeedlemanVunschForMultipleStrings setMatrixOfStrings(ArrayList<char[]> matrixOfStrings) {
        this.matrixOfStrings = matrixOfStrings;
        return this;
    }

    private int[] clean(int[] matr) {
        for (int i = 0; i < matr.length; i++) {
            matr[i] = 0;
        }
        return matr;
    }

    public int[] solveMatrix() {
        int[][] matrixForPair;
        int[] indexes = new int[numOfStrings];
        for (int i = 0; i < numOfStrings; i++) {
            for (int j = i+1; j < numOfStrings; j++) {
                NeedlemanVunsch pair = new NeedlemanVunsch(matrixOfStrings.get(i), matrixOfStrings.get(j));
                matrixForPair = pair.calculateEvaluation().getMatrix();
                indexes = clean(indexes);
                for (int k = 0; k < sizeOfString; k++) {
                    indexes[j] = k;
                    for (int m = 0; m < sizeOfString; m++) {
                        indexes[i] = m;
                        scoreMatrix[getElemOfScoreMatrix(indexes)] = matrixForPair[k][m];
                    }
                }
            }
        }
        return scoreMatrix;
    }
}
