package Fasta;

import Tables.Tables;

public final class Diagonal {
    private int x;
    private int y;
    private int score;
    private int index;
    private int length;

    public Diagonal(Region region, int ext) {
        int a = region.getDiagonal() - ext + 1;
        if(a > 0) {
            x = region.getFront() + a;
            y = region.getFront();
        } else {
            x = region.getFront();
            y = region.getFront() + a;
        }
        length = region.getBack() - region.getFront();
        score = region.getScore();
        index = region.getDiagonal();
    }

    public boolean compareWith(Diagonal diagonal) {
        return x + length < diagonal.getX() + diagonal.getLength() &&
                y + length < diagonal.getY() + diagonal.getLength();
    }

    public void calculateScore(char[] first, char[] second) {
        if(length - 1 + x > second.length || length - 1 + y > second.length) {
            return;
        }
        score = 0;
        for (int i = 0; i < length; i++) {
            score += Tables.getSimilarity(first[i + x], second[i + y]);
        }
    }

    public int getLength() {
        return length;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
