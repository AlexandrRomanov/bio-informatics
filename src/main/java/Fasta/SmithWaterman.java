package Fasta;

import Tables.Tables;

import java.util.Map;

public class SmithWaterman {
    private static int GAP = -1;
    private static int maxOfFour(int a, int b, int c, int d) {
        return Math.max(0, Math.max(b, Math.max(c, d)));
    }

    public static int calculateSmithWaterman(char[] first, char[] second, Diagonal diagonal, int ban)
    {
        int score = 0;
        int[][] H = new int[first.length + 1][second.length + 1];
        int z = Math.abs(diagonal.getX() - diagonal.getY());
        for (int i = 1; i <= first.length; i++) {
            for (int j = 1; j <= second.length; j++) {
                if(Math.abs(i - j) < z + ban) {
                    H[i][j] = maxOfFour(0,
                            H[i-1][j] - GAP,
                            H[i][j - 1] - GAP,
                            H[i-1][j-1] + Tables.getSimilarity(first[i], second[j]));
                    score += H[i][j];
                }
            }
        }
        return score;
    }
}
