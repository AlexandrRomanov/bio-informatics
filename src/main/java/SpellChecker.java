
public class SpellChecker {

    public char[] changeSpelling(String str)
    {
        str = str.toUpperCase();
        return str.toCharArray();
    }

    public boolean checkLetters(char[] str) {
        for (int i = 0; i < str.length; i++) {
            if(str[i] < 'A' || str[i] > 'Z' ||
                    str[i] == 'U' || str[i] == 'B'|| str[i] == 'J'|| str[i] == 'O'|| str[i] == 'X'|| str[i] == 'Z')
            {
                return false;
            }
        }
        return true;
    }
}
