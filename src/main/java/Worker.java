import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Worker {

    private static int maxLength;

    private static ArrayList<char[]> getStrings() {
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter strings");
        String ans;
        SpellChecker checker = new SpellChecker();
        ArrayList<char[]> str = new ArrayList<char[]>();
        while (true){
            ans = in.next();
            if(ans.toLowerCase().equals("done"))
            {
                return str;
            }
            if (checker.checkLetters(checker.changeSpelling(ans))) {
                str.add(checker.changeSpelling(ans));
                if(ans.length() > maxLength) {
                    maxLength = ans.length();
                }
            } else {
                System.out.println("Error, wrong string.\n Try other string.");
                System.out.println("If you want end this, write Done");
            }
        }
    }

    public static void main(final String[] args) {
        Scanner in = new Scanner(System.in);
        ArrayList<char[]> strings = getStrings();
        NeedlemanVunschForMultipleStrings mainwork = new NeedlemanVunschForMultipleStrings(maxLength, strings.size());
        if (strings.size() == 2) {
            System.out.println("Do you want to calculate the checksum or alignment");
            while (true) {
                String ans = in.next();
                if (ans.toLowerCase().equals("checksum") || ans.toLowerCase().equals("alignment")) {
                    if (ans.toLowerCase().equals("checksum")) {
                        System.out.println(mainwork.setMatrixOfStrings(strings).solveMatrix()[(int)Math.pow(maxLength, strings.size())]);
                    } else {
                        System.out.println(new NeedlemanVunsch(strings.get(0), strings.get(1)).calculateEvaluation().findChange());
                    }
                } else {
                    continue;
                }
                break;
            }
        } else {
            System.out.println(mainwork.setMatrixOfStrings(strings).solveMatrix()[124]);
        }
    }
}
